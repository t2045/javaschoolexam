# README #

This is a repo with T-Systems Java School preliminary examination tasks.

The exam includes 3 tasks to be done: [Calculator](/tasks/Calculator.md), [Pyramid](/tasks/Pyramid.md), and
[Subsequence](/tasks/Subsequence.md)

### Result ###

* Author name :  Ivanov Bogdan
* GITLAB : ![GITLAB Status build javaschoolexam](https://raw.githubusercontent.com/elbosso/anybadge/f963596a76577433199df00c34c2cef019da63e8/examples/pipeline.svg)